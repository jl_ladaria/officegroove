# Important notice

Due to storage constraints, this repository is now obsolete. Please refer to the following link for the lastest GIT version of the project:
<a href="https://dev.azure.com/jlladaria/OfficeGroove">https://dev.azure.com/jlladaria/OfficeGroove</a>

# README

<div align="center">
  <img src="Content/Splash/Splash.jpg" alt="logo" />
  <br/>

  <h3><b>Office Groove: Agile Disruption</b></h3>

</div>

Authors <a name="authors"></a>

**Juan Luis Ladaria Sanchez**

- GitLab: [@jl_ladaria](https://gitlab.com/jl_ladaria)

# OfficeGroove

The aim of this project is to create a linear 3D platform video game, with levels made up of variable modular components. To complete the levels, the player will have to overcome obstacles within a corridor-type route until they reach the end (the goal). There will be two types of obstacles; static (non-interactive, can only be avoided) and enemy (can be avoided or interacted with, to defeat it). On the other hand, the game will have a humorous theme, where the typical business environment within an office will be mocked.

Thus, the project is aimed at an adult audience, immersed in today’s business world, that yet seek to relax or have fun for a brief moment. The problem arises within these users; they have little time to play and, in addition, they have a very variable level of skill or experience with video games.

Therefore, in order to reach a larger audience, the procedural generation of levels will serve to establish an adaptive difficulty. In other words, the different instances, obstacles or aids that are generated must be conditioned by the efficiency of the player.

This efficiency or skill that each Player has is measured by three factors; the "life" points consumed, the obstacles surpassed and the time within the level. Depending on these parameters, changes will conditionally occur on the generated path each time the player moves further into the game; it will increase or reduce the number of life points that can be collected, increase or reduce certain types of obstacles or even create an alternative way out (to artificially reduce the level’s length and duration, if needed).

A customized difficulty for the player will result in a softer learning curve and less time needed to advance within the game, while also increasing the feeling of accomplishment, and avoiding incurring in frustration.

# Build 

Developed with Unreal Engine 5.3

# Requirements

Unreal Engine 5.3

Microsoft Visual Studio as per: <a href="https://dev.epicgames.com/documentation/en-us/unreal-engine/setting-up-visual-studio-development-environment-for-cplusplus-projects-in-unreal-engine?application_version=5.0">this setup</a>

# How to deploy

Copy all files into a project folder with your PC, open Unreal Engine 5.3 and load file OfficeGroove.uproject as new project or open directly by double clicking.